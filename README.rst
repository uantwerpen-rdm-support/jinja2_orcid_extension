======================
Jinja2 ORCID extension
======================

Extension with custom filters replacing ORCIDs with elements 
from the orcid records.


* Free software: MIT license
* Documentation: https://jinja2-orcid-extension.readthedocs.io.

Before we begin...
------------------

It is important to note that this package was created for 
one specific use case, namely to be used as a jinja2 
extension while generating project structures with cookiecutter 
through the cookiecutter CLI interface. The goal is to make 
the information in the ORCID records available for researchers 
who are using cookiecutter for data management puposes, 
but are otherwise unfamiliar with programming.

The package works by adding custom jinja2 filters that perform 
a call to the ORCID REST API at the time of template rendering. 
Using this package in another context (e.g. in the context
of a web framework), will likely be a bad idea. Fetching data 
in this way will not be in lign with the design principles of 
the framework you are using. Using it in those contexts will 
make error handling, automated testing and debugging more difficult.


Features
--------

The package contains filter expressions which you can use on valid 
ORCIDS in Jinja2 templates. Each filter replaces the ORCID in 
the template with a string containing certain elements from
the ORCID record. Currently several filters are available:

* full_credit: returns a single preformated label containing the credit name and orcid id.
* credit_name: contains the credit name as supplied in the ORCID record
* given_names: contains the given names as supplied in the ORCID record
* family_name: contains the family name as supplied in the ORCID record

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
