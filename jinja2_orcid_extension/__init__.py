"""Top-level package for Jinja2 ORCID extension."""
from .jinja2_orcid_extension import OrcidExtension

__author__ = """Maarten Vermeyen"""
__email__ = 'maarten@vermeyen.xyz'
__version__ = '0.1.0.dev1'
__all__ = ['OrcidExtension', ]
